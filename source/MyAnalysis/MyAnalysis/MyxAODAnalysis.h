#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <MuonSelectorTools/MuonSelectionTool.h>
#include "xAODTrigMuon/L2StandAloneMuonContainer.h"
#include "xAODTrigMuon/L2CombinedMuonContainer.h"
#include "xAODTrigger/MuonRoIContainer.h"
#include "TruthClassification/TruthClassificationTool.h"

#include "xAODMuon/MuonContainer.h"

class MyxAODAnalysis : public EL::AnaAlgorithm
{
 public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

 private:

  StatusCode fillVariablesPer3OfflineMuon(const std::vector<const xAOD::Muon*> trimuon, const xAOD::Muon* tag_mu , const std::string tag_chain, const std::string probe_chain) const;
  bool isMatchedEFCB(const xAOD::Muon* muEFCB , const xAOD::Muon* muoff , double &dR)const;
  bool isMatchedL2SA(const xAOD::L2StandAloneMuon* muL2 , const xAOD::Muon* muOff,double &dR)const;
  bool searchProbemupair(const std::vector<const xAOD::Muon*> mu_pair,std::vector<const xAOD::L2StandAloneMuon*> &probe_L2SA_mu, const std::string probe_chain) const;
  bool isolationTagProbe(const xAOD::Muon* tag_mu , const std::vector<const xAOD::Muon*> mu_pair)const;
  bool isFromJpsi(const std::vector<const xAOD::Muon*> mu_pair)const;
  double calc_dphi(const double phi1, const double phi2)const;

  bool searchProbeL2SAmupair(const std::vector<const xAOD::Muon*> mu_pair, std::vector<const xAOD::L2StandAloneMuon*> &probe_L2SA_mu, const std::string probe_chain) const;


  /* ToolHandle<TrigConf::ITrigConfigTool> m_trigConfigTool; */
  ToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool;
  ToolHandle<CP::MuonSelectionTool> m_selTool;
  ToolHandle<CP::IClassificationTool> m_truthClassificationTool;

  SG::ReadHandleKey<xAOD::EventInfo> m_eventInfo{this, "EventInfoContName", "EventInfo", "event info key"};
  SG::ReadHandleKey<xAOD::L2CombinedMuonContainer> m_L2CBKey{this, "xAODL2CBContainer", "HLT_MuonL2CBInfo", "Name of L2CBContainer object"};
  SG::ReadHandleKey<xAOD::L2CombinedMuonContainer> m_L2CBIOKey{this, "xAODL2CBContainer_IO", "HLT_MuonL2CBInfoIOmode", "Name of L2CBIOContainer object"};
  SG::ReadHandleKey<xAOD::L2StandAloneMuonContainer> m_L2SAMTKey{this, "xAODL2SAContainer_MT", "HLT_MuonL2SAInfol2mtmode", "Name of L2SAMTContainer object"};
  SG::ReadHandleKey<xAOD::L2StandAloneMuonContainer> m_L2SAKey{this, "xAODL2SAContainer", "HLT_MuonL2SAInfo", "Name of L2SAContainer object"};
  
  SG::ReadHandleKey<xAOD::L2StandAloneMuonContainer> m_L2SAIOKey{this, "xAODL2SAContainer_IO", "HLT_MuonL2SAInfoIOmode", "Name of L2SAIOContainer object"};
  SG::ReadHandleKey<xAOD::MuonContainer> m_MuonContainerKey {this, "MuonContainerName", "Muons", "Offline muon container"};

  Gaudi::Property<int> m_muontype {this, "MuonType", xAOD::Muon::MuonType::Combined, "MuonType used for monitoring"};
  Gaudi::Property<std::vector<std::string> > m_monitored_chains {this, "MonitoredChains", {}, "Trigger chains that are monitored"};

  unsigned int m_runNumber = 0;
  unsigned int m_lumiBlock = 0;
  unsigned long long m_eventNumber = 0;
  float m_mcEventWeight = 0;
  float m_actualInteractionsPerCrossing = 0;
  float m_averageInteractionsPerCrossing = 0;

  //L2io match
  std::unique_ptr<std::vector<float>> m_probe_pt;
  std::unique_ptr<std::vector<float>> m_probe_eta;
  std::unique_ptr<std::vector<float>> m_probe_phi;

  std::unique_ptr<std::vector<float>> m_probe_L1_pt;
  std::unique_ptr<std::vector<float>> m_probe_L1_eta;
  std::unique_ptr<std::vector<float>> m_probe_L1_phi;

  std::unique_ptr<std::vector<float>> m_probe_L2sa_pt;
  std::unique_ptr<std::vector<float>> m_probe_L2sa_eta;
  std::unique_ptr<std::vector<float>> m_probe_L2sa_phi;

  std::unique_ptr<std::vector<float>> m_probe_L2io_pt;
  std::unique_ptr<std::vector<float>> m_probe_L2io_eta;
  std::unique_ptr<std::vector<float>> m_probe_L2io_phi;
  
  std::unique_ptr<std::vector<float>> m_tag_pt;
  std::unique_ptr<std::vector<float>> m_tag_eta;
  std::unique_ptr<std::vector<float>> m_tag_phi;

  std::unique_ptr<std::vector<float>> m_tag_EFCB_pt;
  std::unique_ptr<std::vector<float>> m_tag_EFCB_eta;
  std::unique_ptr<std::vector<float>> m_tag_EFCB_phi;

  std::unique_ptr<std::vector<float>> m_denominator;
  std::unique_ptr<std::vector<float>> m_numerator;
  std::unique_ptr<std::vector<float>> m_off_dR;

  std::unique_ptr<std::vector<bool>> m_isMatch_tag;
  std::unique_ptr<std::vector<bool>> m_isMatch_probe;
  std::unique_ptr<std::vector<bool>> m_isIsolation;

  std::unique_ptr<std::vector<bool>> m_isPass_tag;
  std::unique_ptr<std::vector<bool>> m_isPass_probe;
  std::unique_ptr<std::vector<bool>> m_isFromJpsi;


  /* std::unique_ptr<std::vector<float>> m_road_Aw; */
  /* std::unique_ptr<std::vector<float>> m_road_Bw; */
  /* std::unique_ptr<std::vector<float>> m_road_R; */
  /* std::unique_ptr<std::vector<float>> m_road_Z; */
  /* std::unique_ptr<std::vector<float>> m_road_eta; */
  /* std::unique_ptr<std::vector<float>> m_road_phi; */

  /* std::unique_ptr<std::vector<float>> m_tgcHitEta; */
  /* std::unique_ptr<std::vector<float>> m_tgcHitPhi; */
  /* std::unique_ptr<std::vector<float>> m_tgcHitR; */
  /* std::unique_ptr<std::vector<float>> m_tgcHitZ; */
  /* std::unique_ptr<std::vector<int>> m_tgcHitStationNum; */
  /* std::unique_ptr<std::vector<bool>> m_tgcHitIsStrip; */
  /* std::unique_ptr<std::vector<int>> m_tgcHitBCTag; */
  /* std::unique_ptr<std::vector<bool>> m_tgcHitInRoad; */

};

#endif
