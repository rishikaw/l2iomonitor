#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <math.h>

#include <TH1.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTrigMuon/L2StandAloneMuonContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/xAODTruthHelpers.h>
#include "xAODTrigMuon/TrigMuonDefs.h"


MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
    // m_trigConfigTool ("TrigConf::xAODConfigTool/xAODConfigTool",this),
    m_trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool",this),
    m_selTool ("CP::MuonSelectionTool", this),
    m_truthClassificationTool("TruthClassificationTool/TruthClassificationTool", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  // declareProperty("trigConfigTool", m_trigConfigTool, "the TrigConf::xAODConfig tool");
  declareProperty("trigDecisionTool", m_trigDecisionTool, "the TrigDecisio tool");
  declareProperty("MuonSelTool", m_selTool, "The muon selection tool");
  declareProperty("truthClassificationTool", m_truthClassificationTool, "IFF TruthClassification");
}




StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  // ANA_CHECK( m_trigConfigTool.retrieve() );
  ANA_CHECK( m_trigDecisionTool.retrieve() );
  ANA_CHECK( m_selTool.retrieve() );

  ATH_CHECK( m_truthClassificationTool.retrieve() );

  ATH_CHECK(m_eventInfo.initialize());

  ANA_CHECK( m_L2CBKey.initialize() );
  ANA_CHECK( m_L2CBIOKey.initialize() );
  ANA_CHECK( m_L2SAMTKey.initialize() );
  ANA_CHECK( m_L2SAKey.initialize() );
  ANA_CHECK( m_L2SAIOKey.initialize() );
  ANA_CHECK( m_MuonContainerKey.initialize());

  ANA_CHECK (book (TTree ("analysis", "My analysis tree")));
  TTree* mytree = tree ("analysis");

  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("LumiBlock", &m_lumiBlock);
  mytree->Branch ("EventNumber", &m_eventNumber);
  mytree->Branch ("mcEventWeight", &m_mcEventWeight);
  mytree->Branch ("actualInteractionsPerCrossing", &m_actualInteractionsPerCrossing);
  mytree->Branch ("averageInteractionsPerCrossing", &m_averageInteractionsPerCrossing);

  //L2SAIO muon monitoring
  m_probe_pt  = std::make_unique<std::vector<float>>();
  m_probe_eta  = std::make_unique<std::vector<float>>();
  m_probe_phi  = std::make_unique<std::vector<float>>();

  m_probe_L1_pt  = std::make_unique<std::vector<float>>();
  m_probe_L1_eta  = std::make_unique<std::vector<float>>();
  m_probe_L1_phi  = std::make_unique<std::vector<float>>();

  m_tag_pt  = std::make_unique<std::vector<float>>();
  m_tag_eta  = std::make_unique<std::vector<float>>();
  m_tag_phi  = std::make_unique<std::vector<float>>();

  m_tag_EFCB_pt  = std::make_unique<std::vector<float>>();
  m_tag_EFCB_eta  = std::make_unique<std::vector<float>>();
  m_tag_EFCB_phi  = std::make_unique<std::vector<float>>();

  m_denominator = std::make_unique<std::vector<float>>();
  m_numerator = std::make_unique<std::vector<float>>();
  m_off_dR = std::make_unique<std::vector<float>>();

  m_isPass_tag = std::make_unique<std::vector<bool>>();
  m_isPass_probe = std::make_unique<std::vector<bool>>();
  m_isFromJpsi = std::make_unique<std::vector<bool>>();

  m_isMatch_tag = std::make_unique<std::vector<bool>>();
  m_isMatch_probe = std::make_unique<std::vector<bool>>();
  m_isIsolation = std::make_unique<std::vector<bool>>();


  mytree->Branch ("probe_pt", &*m_probe_pt);
  mytree->Branch ("probe_eta", &*m_probe_eta);
  mytree->Branch ("probe_phi", &*m_probe_phi);

  mytree->Branch ("probe_L1_pt", &*m_probe_L1_pt);
  mytree->Branch ("probe_L1_eta", &*m_probe_L1_eta);
  mytree->Branch ("probe_L1_phi", &*m_probe_L1_phi);

  mytree->Branch ("tag_pt", &*m_tag_pt);
  mytree->Branch ("tag_eta", &*m_tag_eta);
  mytree->Branch ("tag_phi", &*m_tag_phi);

  mytree->Branch ("tag_EFCB_pt", &*m_tag_EFCB_pt);
  mytree->Branch ("tag_EFCB_eta", &*m_tag_EFCB_eta);
  mytree->Branch ("tag_EFCB_phi", &*m_tag_EFCB_phi);

  mytree->Branch ("denominator_pass", &*m_denominator);
  mytree->Branch ("numerator_pass", &*m_numerator);
  mytree->Branch ("off_dR", &*m_off_dR);

  mytree->Branch ("isPass_tag", &*m_isPass_tag);
  mytree->Branch ("isPass_probe", &*m_isPass_probe);
  mytree->Branch ("isFromJpsi", &*m_isFromJpsi);

  mytree->Branch ("isMatch_tag", &*m_isMatch_tag);
  mytree->Branch ("isMatch_probe", &*m_isMatch_probe);
  mytree->Branch ("isIsolation", &*m_isIsolation);

  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //if(!m_trigDecisionTool->isPassed("HLT_mu24_ivarmedium_L1MU14FCH")) return StatusCode::SUCCESS;

  // Get the truth Muons from the event:
  // const xAOD::TruthParticleContainer* truthmuons = 0;
  // ANA_CHECK(evtStore()->retrieve( truthmuons, "TruthMuons" ) );
  SG::ReadHandle<xAOD::EventInfo> eventInfo(m_eventInfo);

  m_runNumber = eventInfo->runNumber();
  m_lumiBlock = eventInfo->lumiBlock();
  m_eventNumber = eventInfo->eventNumber();
  m_mcEventWeight = eventInfo->mcEventWeight();
  m_actualInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing();
  m_averageInteractionsPerCrossing = eventInfo->actualInteractionsPerCrossing();


  m_probe_pt ->clear();
  m_probe_eta ->clear();
  m_probe_phi ->clear();

  m_probe_L1_pt ->clear();
  m_probe_L1_eta ->clear();
  m_probe_L1_phi ->clear();

  m_tag_pt ->clear();
  m_tag_eta ->clear();
  m_tag_phi ->clear();

  m_tag_EFCB_pt ->clear();
  m_tag_EFCB_eta ->clear();
  m_tag_EFCB_phi ->clear();

  m_denominator -> clear();
  m_numerator -> clear();
  m_off_dR -> clear();

  m_isPass_tag -> clear();
  m_isPass_probe -> clear();
  m_isFromJpsi -> clear();

  m_isMatch_tag -> clear();
  m_isMatch_probe -> clear();
  m_isIsolation -> clear();

  // auto chainGroup = m_trigDecisionTool->getChainGroup("HLT_mu.*");
  // std::map<std::string,int> triggerCounts;
  // for(auto &trig : chainGroup->getListOfTriggers()) {
  //   auto cg = m_trigDecisionTool->getChainGroup(trig);
  //   std::string thisTrig = trig;
  //   ANA_MSG_INFO ("execute(): " << thisTrig << ", chain passed(1)/failed(0) = " << cg->isPassed() << ", total chain prescale (L1*HLT) = " << cg->getPrescale());
  // }

  // ATH_MSG_INFO( m_trigDecisionTool->getNavigationFormat() );


  ANA_MSG_INFO ("LB " << eventInfo->lumiBlock() << " eventNum " << eventInfo->eventNumber());

  const std::string targetTrig = "HLT_mu24_ivarmedium_L1MU14FCH"; //"HLT_mu26_ivarmedium_mu6_l2io_probe_L1MU14FCH"; //"HLT_mu24_ivarmedium_mu4_probe_L1MU14FCH"; //"HLT_mu10_L1MU8F";//"HLT_mu60_0eta105_msonly_L1MU14FCH";//"HLT_mu6_L1MU5VF"; //




  const EventContext& ctx = getContext();
  //ATH_MSG_INFO("Get event context << " << ctx );

  //ATH_MSG_INFO("l2io TP trigger isPassed? " << m_trigDecisionTool->isPassed("HLT_mu26_ivarmedium_mu6_l2io_probe_L1MU14FCH"));

  SG::ReadHandle<xAOD::L2StandAloneMuonContainer> l2sa_inCntn( m_L2SAKey, ctx );
  if( l2sa_inCntn.isValid() ){/*ATH_MSG_DEBUG("evtStore() is not contain L2cb_inCntn" << m_L2CBKey);*/}
  SG::ReadHandle<xAOD::L2StandAloneMuonContainer> l2saio_inCntn( m_L2SAIOKey, ctx );
  if( l2saio_inCntn.isValid() ){/*ATH_MSG_DEBUG("evtStore() is not contain L2cb_inCntn" << m_L2CBKey);*/}
  SG::ReadHandle<xAOD::L2CombinedMuonContainer> l2cbio_inCntn( m_L2CBIOKey, ctx );
  if( l2cbio_inCntn.isValid() ){/*ATH_MSG_DEBUG("evtStore() is not contain L2cb_inCntn" << m_L2CBKey);*/}
  SG::ReadHandle<xAOD::L2CombinedMuonContainer> l2cb_inCntn( m_L2CBKey, ctx );
  if( !l2cb_inCntn.isValid() ){/*ATH_MSG_DEBUG("evtStore() is not contain L2cb_inCntn" << m_L2CBKey);*/}

  const std::string &chain = "HLT_mu24_ivarmedium_mu6_l2io_probe_L1MU14FCH";
  
  SG::ReadHandle<xAOD::MuonContainer> muons(m_MuonContainerKey, ctx);
  
  if (! muons.isValid() ) {
    ATH_MSG_ERROR("evtStore() does not contain muon Collection with name "<< m_MuonContainerKey);
    //return StatusCode::FAILURE
  }

  const xAOD::Muon* tag_mu = nullptr;
  std::vector<const xAOD::Muon*> trimuon;

  double mu_maxPt = 0;

  /*begin muon selection and fill probes*/
  for (const xAOD::Muon * mu : *muons) {
    if(mu->muonType()<=m_muontype 
    && (mu->author()==xAOD::Muon::Author::MuidCo)
    && m_selTool->getQuality(*mu)<=xAOD::Muon::Medium && mu->pt()/1e3 > 10)
    {
      if(mu_maxPt < mu ->pt())
      {
        mu_maxPt = mu->pt();
        tag_mu = mu;
      }

      trimuon.push_back(mu);
    }
  }


  const std::string tag_chain = "HLT_mu24_ivarmedium_L1MU14FCH";
  const std::string probe_chain = "HLT_2mu10_bJpsimumu_L12MU8F";

  if(trimuon.size() == 3 && tag_mu -> pt()/1e3 >= 10)
  {
    ATH_MSG_DEBUG("This event contain "<< trimuon.size()<<" muons... and muons max pt is: " << mu_maxPt/1e3);
    ATH_CHECK(fillVariablesPer3OfflineMuon(trimuon,tag_mu,tag_chain,probe_chain));

  }

  tree ("analysis")->Fill ();

  return StatusCode::SUCCESS;
  
}

StatusCode MyxAODAnalysis :: fillVariablesPer3OfflineMuon(const std::vector<const xAOD::Muon*> trimuon,const xAOD::Muon* tag_mu, const std::string tag_chain, const std::string probe_chain) const
{
  int denominator_pass = 0;
  int numerator_pass = 0;

  std::vector<const xAOD::Muon*> mu_pair;
  for(const xAOD::Muon* mu : trimuon)
  {
      if(tag_mu != mu) { mu_pair.push_back(mu);} 
  }

  const xAOD::Muon* tag_EFCB_mu = nullptr;
  std::vector<const xAOD::L2StandAloneMuon*> probe_L2SA_mu ;

  if(m_trigDecisionTool->isPassed(tag_chain)) 
  { ATH_MSG_DEBUG("This event pass tag chain"); }
  if(m_trigDecisionTool->isPassed(probe_chain))
  { ATH_MSG_DEBUG("This event pass probe chain"); }
    
  //match tag and EFCB muon
  bool tag_matched = false;
  double dR = 0.;
  double min_dR = 1000.;
  std::vector< TrigCompositeUtils::LinkInfo<xAOD::MuonContainer> > featureCont = m_trigDecisionTool->features<xAOD::MuonContainer>( tag_chain, TrigDefs::Physics, "HLT_MuonsCB_.*");
  for (const TrigCompositeUtils::LinkInfo<xAOD::MuonContainer>& muEFCBLinkInfo : featureCont)
  {
    if(!muEFCBLinkInfo.isValid()) { continue; }
    const ElementLink<xAOD::MuonContainer> muEFCB = muEFCBLinkInfo.link;
    if(isMatchedEFCB((*muEFCB), tag_mu, dR )) 
    {
      tag_matched = true;
      if(dR < min_dR)
      {
        tag_EFCB_mu = (*muEFCB);
      }
    }
  }
  //if(tag_matched != true ){ return StatusCode::SUCCESS; }

  //require tag and probe muon isolation
  //if( !isolationTagProbe(tag_mu,mu_pair) ) { return StatusCode::SUCCESS; }
  bool isolation = isolationTagProbe(tag_mu,mu_pair);

  //match probe and L1 muon pair
  bool probe_matched = searchProbemupair(mu_pair,probe_L2SA_mu,probe_chain);
  ANA_MSG_DEBUG("Matching result of Tag muon: " << tag_matched << "  probe muon: "<< probe_matched );

  bool probe_matchedL2SA = searchProbeL2SAmupair(mu_pair,probe_L2SA_mu,probe_chain);
  //muon pair is from Jpsi?
  //if(isFromJpsi(mu_pair)){}


  //fill variable
  if(tag_matched == true && probe_matched == true)
  {
    ANA_MSG_DEBUG("This event satisfies the requirements of denominator");
    denominator_pass = 1;
    if(m_trigDecisionTool->isPassed(probe_chain))
    {
      ATH_MSG_DEBUG("This event satisfies the requirements of numerator");
      numerator_pass = 1;
    }
  }

  m_tag_pt -> push_back(tag_mu->pt());
  m_tag_eta -> push_back(tag_mu->eta());
  m_tag_phi -> push_back(tag_mu->phi());

  if(tag_matched == true)
  {
    m_tag_EFCB_pt -> push_back(tag_EFCB_mu->pt());
    m_tag_EFCB_eta -> push_back(tag_EFCB_mu->eta());
    m_tag_EFCB_phi -> push_back(tag_EFCB_mu->phi());
  }

  for(const xAOD::Muon *mu : mu_pair)
  {
    m_probe_pt -> push_back(mu -> pt());
    m_probe_eta -> push_back(mu -> eta());
    m_probe_phi -> push_back(mu -> phi());
  }

  if(probe_matched == true)
  {
    for(const xAOD::L2StandAloneMuon* L2SAmu : probe_L2SA_mu)
    {
      m_probe_L1_eta -> push_back(L2SAmu -> roiEta());
      m_probe_L1_phi -> push_back(L2SAmu -> roiPhi());
    }
  }
  m_isIsolation -> push_back(isolation);

  ATH_MSG_DEBUG("denomnator and numerator pass: " << denominator_pass << ", "<< numerator_pass);
  m_denominator->push_back(denominator_pass);
  m_numerator->push_back(numerator_pass);

  m_isPass_tag -> push_back(m_trigDecisionTool->isPassed(tag_chain));
  m_isPass_probe -> push_back(m_trigDecisionTool->isPassed(probe_chain));  
  m_isFromJpsi -> push_back(isFromJpsi(mu_pair));

  m_isMatch_tag -> push_back(tag_matched);
  m_isMatch_probe -> push_back(probe_matched);

  return StatusCode::SUCCESS;
}

bool MyxAODAnalysis:: isolationTagProbe(const xAOD::Muon* tag_mu , const std::vector<const xAOD::Muon*> mu_pair)const
{
  bool isolation = true;
  double dR_threshold = 0.4;
  

  for(const xAOD::Muon* mu : mu_pair)
  {
    double dphi = calc_dphi(tag_mu->phi() , mu->phi());
    double deta = tag_mu->eta() - mu->eta();
    double dR = abs(dphi*dphi + deta*deta);
    if(dR < dR_threshold)
    {
      isolation = false;
    }
  }
  return isolation;
}

bool MyxAODAnalysis:: isFromJpsi(const std::vector<const xAOD::Muon*> mu_pair)const
{
  if(mu_pair.size() != 2) {return false; }
  bool Jpsi = false;
  std::vector<float>Jpsi_massThreshold = {2500.0 , 4300.0}; //MeV
  std::vector<TLorentzVector> muLV;
  for(const xAOD::Muon *mu : mu_pair)
  {
    TLorentzVector LV;
    LV.SetPtEtaPhiM(mu->pt(),mu->eta(),mu->phi(),0 );
    muLV.push_back(LV);
  } 
  double dimuMass = (muLV.at(0) + muLV.at(1)).M();
  if(dimuMass > Jpsi_massThreshold[0] && dimuMass < Jpsi_massThreshold[1])
  {
    Jpsi = true;
  }
  return Jpsi;
}

bool MyxAODAnalysis:: isMatchedEFCB(const xAOD::Muon* muEFCB , const xAOD::Muon* muoff , double &dR)const
{
  bool isMatched = false;
  double dR_threshold = 0.2;
  double dphi = calc_dphi(muEFCB->phi() , muoff->phi());
  double deta = muEFCB ->eta() - muoff->eta();
  dR = abs(dphi*dphi + deta*deta);
  //ATH_MSG_DEBUG("dR of EFCB muon and offline muon:" << dR);
  if(dR < dR_threshold)
  {
    isMatched = true;
  }
  return isMatched;
}


bool MyxAODAnalysis::isMatchedL2SA(const xAOD::L2StandAloneMuon* muL2 , const xAOD::Muon* muOff , double &dR)const
{
  bool isMatched = false;
  double dR_threshold = 0.2;

  double deta = muL2->roiEta() - muOff->eta();
  double dphi = calc_dphi(muL2->roiPhi() , muOff->phi());
  dR = abs(deta*deta + dphi*dphi);
  //ATH_MSG_DEBUG("dR of L2SA muon and offline muon: " << dR);
  if(dR < dR_threshold)
  {
    isMatched = true;
  }

  return isMatched;
}

double MyxAODAnalysis::calc_dphi(const double phi1, const double phi2)const
{
  return  -remainder( -phi1 + phi2, 2*M_PI );
}

bool MyxAODAnalysis :: searchProbemupair(const std::vector<const xAOD::Muon*> mu_pair, std::vector<const xAOD::L2StandAloneMuon*> &probe_L2SA_mu, const std::string probe_chain) const
{
  bool Probepair_isMatched = false;
  if(mu_pair.size() != 2) { return false; }

  std::vector<std::vector<double>> dR_arr = {{1000.,1000.},{1000.,1000.}};
  std::vector<std::vector<const xAOD::L2StandAloneMuon* >> L2mu_arr = {{nullptr,nullptr},{nullptr,nullptr}};
  for(int n = 0; n < 2 ; n++ )
  {
    const xAOD::Muon *mu = mu_pair.at(n);
    for(int LegIndex = 0; LegIndex < 2 ; LegIndex++)
    {
      double dR;
      double dR_min = 1000.;
      std::vector< TrigCompositeUtils::LinkInfo<xAOD::L2StandAloneMuonContainer> > featureCont_Index = m_trigDecisionTool->features<xAOD::L2StandAloneMuonContainer>
                                                                                    (
                                                                                    probe_chain ,
                                                                                    TrigDefs::includeFailedDecisions,
                                                                                    "HLT_MuonL2SAInfo",
                                                                                    TrigDefs::lastFeatureOfType,
                                                                                    TrigCompositeUtils::featureString(),
                                                                                    LegIndex
                                                                                    );
      for(const TrigCompositeUtils::LinkInfo<xAOD::L2StandAloneMuonContainer>& L2SALinkInfo : featureCont_Index)
      {
        if(!L2SALinkInfo.isValid()){ continue; }
        const ElementLink<xAOD::L2StandAloneMuonContainer> L2SAobject = L2SALinkInfo.link;
        if(isMatchedL2SA((*L2SAobject),mu,dR))
        {
          if(dR_min > dR)
          {
            dR_arr.at(n).at(LegIndex) = dR;
            L2mu_arr.at(n).at(LegIndex) = (*L2SAobject);
            dR_min = dR;
          }
        }
      }
    }
  }
  ATH_MSG_DEBUG("dR is " << dR_arr);
  if(dR_arr.at(0).at(1)+dR_arr.at(1).at(0) < dR_arr.at(0).at(0)+dR_arr.at(1).at(1) 
     && dR_arr.at(0).at(1) < 999. && !(dR_arr.at(1).at(0) < 999.))
  {
    probe_L2SA_mu.push_back(L2mu_arr.at(0).at(1));
    probe_L2SA_mu.push_back(L2mu_arr.at(1).at(0));
    Probepair_isMatched = true;
  }
  else if(dR_arr.at(0).at(0) < 999.  && dR_arr.at(1).at(1) < 999.)
  {
    probe_L2SA_mu.push_back(L2mu_arr.at(0).at(0));
    probe_L2SA_mu.push_back(L2mu_arr.at(1).at(1));
    Probepair_isMatched = true;
  }
  return Probepair_isMatched;
}

bool MyxAODAnalysis :: searchProbeL2SAmupair(const std::vector<const xAOD::Muon*> mu_pair, std::vector<const xAOD::L2StandAloneMuon*> &probe_L2SA_mu, const std::string probe_chain) const
{
  ATH_MSG_DEBUG("here is serchProbeL2SAmupair");
  std::vector< TrigCompositeUtils::LinkInfo<xAOD::MuonRoIContainer> > l1Links = m_trigDecisionTool->features<xAOD::MuonRoIContainer>( probe_chain, TrigDefs::includeFailedDecisions, "initialRecRoI" );
  for(const TrigCompositeUtils::LinkInfo<xAOD::MuonRoIContainer>& l1LinkInfo : l1Links)
  {
    ATH_MSG_DEBUG("here is serch probe mu pair for-loop");
    if( !l1LinkInfo.isValid() ) continue;
    const ElementLink<xAOD::MuonRoIContainer> l1 = l1LinkInfo.link;
    if( !l1.isValid() ) continue;
    ATH_MSG_DEBUG ("   L1 eta/phi = " << (*l1)->eta() << "/" << (*l1)->phi());
  }
  return true;
}

StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
