# input data

# testFile = '/gpfs/fs7001/youhei/L2MuonSA/dataset_aod/20230317_L2SApTResolModel/mod_Jpsi/Jpsi_000012/AOD.pool.root'
# import AthenaPoolCnvSvc.ReadAthenaPool
# ServiceMgr.EventSelector.InputCollections = [testFile]

import AthenaPoolCnvSvc.ReadAthenaPool
import os

# data_dir = "/gpfs/fs7001/youhei/L2MuonSA/dataset_aod_official/data23_13p6TeV.00456729.physics_BphysDelayed.merge.AOD.f1370_m2191"
# data_dir = '/home/rishikaw/.snapshots/2023-09-17/data23_13p6TeV.00451896.physics_Main.merge.AOD.f1352_m2171'
#data_dir = "/gpfs/fs8001/rishikaw/data23_13p6TeV.00451896.physics_Main.merge.AOD.f1352_m2171"

#num_file = 1 # you can choose how many files want to read !!

#list_file_name = os.listdir(data_dir)
#list_file_pass = [data_dir + "/" + file_name  for file_name in list_file_name] 

#print("this directory = ",len(list_file_pass))
#ServiceMgr.EventSelector.InputCollections = list_file_pass[0:num_file]


# output file
ServiceMgr += CfgMgr.THistSvc()
ServiceMgr.THistSvc.Output += [
    "ANALYSIS DATAFILE='MyxAODAnalysis.outputs.root' OPT='RECREATE'"
    ]
ServiceMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# Setup TDT
# configTool = CfgMgr.TrigConf__xAODConfigTool("xAODConfigTool")
# ToolSvc += configTool

from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
tdt = Trig__TrigDecisionTool( "TrigDecisionTool",
                              # ConfigTool = configTool,
                              TrigDecisionKey = "xTrigDecision",
                              HLTSummary = "HLTNav_Summary_AODSlimmed",
                              #OutputLevel = 1 
)
tdt.NavigationFormat = "TrigComposite"
# tdt.NavigationFormat = "TriggerElement"
ToolSvc += tdt

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )
alg.OutputLevel = 2

# Add private tool
from AnaAlgorithm.DualUseConfig import addPrivateTool

# muon ID tool
addPrivateTool( alg, 'MuonSelTool', 'CP::MuonSelectionTool' )
alg.MuonSelTool.MuQuality = 1  #0, 1, 2, 3, 4, 5 correspond to Tight, Medium, Loose, VeryLoose, HighPt, LowPtEfficiency
alg.MuonSelTool.IsRun3Geo = True
alg.MuonSelTool.TurnOffMomCorr = True

addPrivateTool( alg, 'truthClassificationTool', 'TruthClassificationTool' )

# Add our algorithm to the main alg sequence
athAlgSeq += alg
