#!/usr/bin/env python

default_submitDir = 'gridJobDir'
inDatasetName = 'data18_13TeV.00358031.physics_Main.merge.AOD.f1184_m2066'
outFileNameBase = 'user.yoyamagu.dataana.20220119_2'

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )

( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()


# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )

# Add our algorithm to the job
# job.algsAdd( alg )

from AnaAlgorithm.DualUseConfig import addPrivateTool

# add the muon selection tool to the algorithm
addPrivateTool( alg, 'MuonSelTool', 'CP::MuonSelectionTool' )
alg.MuonSelTool.MuQuality = 1 # 0 Tight, 1 Medium, 2 Loose, 3 VeryLoose, 4 HighPt, 5 LowPtEfficiency


# create public tools
from AnaAlgorithm.DualUseConfig import createPublicTool

# trigger configuration tool
xAODConfigTool = createPublicTool( 'TrigConf::xAODConfigTool', 'xAODConfigTool' )

# trigger decision tool
TrigDecisionTool = createPublicTool( 'Trig::TrigDecisionTool', 'TrigDecisionTool' )
TrigDecisionTool.ConfigTool = '%s/%s' %(xAODConfigTool.getType(), xAODConfigTool.getName() )
TrigDecisionTool.TrigDecisionKey = 'xTrigDecision'

from AnaAlgorithm.AnaAlgSequence import AnaAlgSequence
seq = AnaAlgSequence( "MyxAODAnalysisSequence" )

seq.addPublicTool(xAODConfigTool)
seq.addPublicTool(TrigDecisionTool)

# xAODConfigTool.OutputLevel = ROOT.MSG.DEBUG
# TrigDecisionTool.OutputLevel = ROOT.MSG.DEBUG

seq.append(alg, inputPropName = None )

## create samplehandler to process CollectionTree
sh = ROOT.SH.SampleHandler()
sh.setMetaString('nc_tree', 'CollectionTree')
         
## scan rucio for dataset
ROOT.SH.scanRucio(sh, inDatasetName)
sh.setMetaString("nc_grid_filter","*")

## create job and feed sample handler and alg
job = ROOT.EL.Job()
job.sampleHandler(sh)
#job.algsAdd( alg )
for alg in seq:
    job.algsAdd (alg)
    pass

job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

## create driver and set output name
driver = ROOT.EL.PrunDriver()
driver.options().setString('nc_outputSampleName', outFileNameBase+'.%in:name[2]%.%in:name[6]%')

driver.submitOnly(job, options.submission_dir)
